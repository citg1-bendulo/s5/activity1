<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import="java.util.Date,java.time.LocalDateTime,java.time.format.DateTimeFormatter"
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>JSP Activity</title>
</head>
<body>

	<!-- Using the scriptlet tag, create a variable dateTime-->
	<!-- Use the LocalDateTime and DateTimeFormatter classes -->
	<!-- Change the pattern to "yyyy-MM-dd HH:mm:ss" -->
  	<%!
  	 	LocalDateTime dateTime = LocalDateTime.now(); 
  		DateTimeFormatter dtf= DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  
  		
  	%>
  	<!-- Using the date time variable declared above, print out the time values -->
  	<!-- Manila = currentDateTime -->
  	<!-- Japan = +1 -->
  	<!-- Germany = -7 -->
  	<!-- You can use the "plusHours" and "minusHours" method from the LocalDateTime class -->
  	<%
  		//added 1 hour for japan time
  		LocalDateTime japanTime= dateTime.plusHours(1);
  				//subtracted 7 hours for germany time
  		LocalDateTime germanyTime= dateTime.minusHours(7);
  	%>
  	<h1>Our Date and Time now is...</h1>
	<ul>
		<!-- mapped the values from the given variables above -->
		<li> Manila:<%=dtf.format(dateTime) %> </li>
		<li> Japan: <%=dtf.format(japanTime) %></li>
		<li> Germany: <%=dtf.format(germanyTime) %> </li>
	</ul>
	
	<!-- Given the following Java Syntax below, apply the correct JSP syntax -->
	<%! 
	private int initVar=3;
	private int serviceVar=3;
	private int destroyVar=3;
	
  	public void jspInit(){
    	initVar--;
    	System.out.println("jspInit(): init"+initVar);
  		}
  	public void jspDestroy(){
    	destroyVar--;
    	destroyVar = destroyVar + initVar;
    	System.out.println("jspDestroy(): destroy"+destroyVar);
  	}
  	%>
  	
  	<%
  	serviceVar--;
  	System.out.println("_jspService(): service"+serviceVar);
  	String content1="content1 : "+initVar;
  	String content2="content2 : "+serviceVar;
  	String content3="content3 : "+destroyVar;
	%>
	
	
	<h1>JSP</h1>
	<p><%=content1 %></p>
	<p><%=content2 %></p>
	<p><%=content3 %></p>

</body>

</html>